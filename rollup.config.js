import ts from 'rollup-plugin-ts';

export default [
	{
    plugins: [
      ts({
        tsconfig: 'tsconfig.json'
      })
    ],
		input: 'src/index.ts',
		watch: true,
		output: { file: 'dist/bundle.js', format: 'es' }
	}
];