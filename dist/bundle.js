var double = function (a) {
    return a * 2;
};

var legacyDouble = function (a) {
    return a * 2;
};

console.log('ts', double(8));
console.log('js', legacyDouble(8));
console.log('global', age);

export { double, legacyDouble };
