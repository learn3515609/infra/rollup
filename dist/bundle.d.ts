import { legacyDouble } from './legacy';
declare const double: (a: number) => number;
export { double, legacyDouble };
