import { double } from './module'

describe('typescript', () => {
  test('double', () => {
    expect(double(4)).toEqual(8)
  })
})

export {}