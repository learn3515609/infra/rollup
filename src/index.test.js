import { legacyDouble } from './legacy/'


describe('legacy', () => {
  test('double', () => {
     expect(legacyDouble(4)).toEqual(8)
  })
})

export {}