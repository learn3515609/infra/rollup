import { double } from './module';
import { legacyDouble } from './legacy';

console.log('ts', double(8));
console.log('js', legacyDouble(8));

console.log('global', age);


export {
  double,
  legacyDouble
}